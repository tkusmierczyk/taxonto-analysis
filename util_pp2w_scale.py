#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
from utils import *
from math import *


if __name__=="__main__":

    inpath = sys.argv[1]
    outpath = sys.argv[2]

    fin = open(inpath)
    fout = open(outpath, "w")

    ff2w = load_pairdict2weight(fin)

    xx2w = {}
    for ff,w in ff2w.iteritems():
        xx2w[ff] = (exp(5*w)-1) / (exp(5)-1)

    store_pairdict2weight(fout, xx2w)
