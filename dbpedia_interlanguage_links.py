#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
from utils import *


def yield_interlangueage_map(fin, lang="es"):
    for parts in load_csv_rows(fin):
        if url_extract_lang(parts[2])!=lang: continue
        source, target = url_extract_name(parts[0]), url_extract_name(parts[2])
        yield (source, target)


if __name__=="__main__":

    fin = sys.stdin
    fout = sys.stdout
    sys.stdout = sys.stderr

    print "The script reads from stdin (unpacked) interlanguage_links_en.ttl"
    print " and prints out to stdout language map (en -> selected language)."

    try:
        lang = sys.argv[1].strip()
    except:
        print "Argument expected: language code (e.g. pl)"
        sys.exit(-1)

    print "Language code: %s" % lang

    for source, target in yield_interlangueage_map(fin, lang):
        fout.write("%s %s\n" % (source, target))

