#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Processing of strings."""


import urllib2
import string
import nltk


def text_filter(text, replace_with = ''):
    """Removes punctuation, digits, specific characters..."""
    #print type(text), text
    text_nopunct = str(text)
    #text_nopunct = str(text).translate(string.maketrans("",""), string.punctuation)
    for c in string.punctuation:
        text_nopunct = text_nopunct.replace(c, replace_with)
    #text_nopunct = text_nopunct.translate(string.maketrans("",""), string.digits)
    for c in string.digits:
        text_nopunct = text_nopunct.replace(c, replace_with)        
    text_nopunct = text_nopunct.replace('”', replace_with)
    text_nopunct = text_nopunct.replace('“', replace_with)
    text_nopunct = text_nopunct.replace('‘', replace_with)
    text_nopunct = text_nopunct.replace('’', replace_with)
    text_nopunct = text_nopunct.replace('\'', replace_with)
    text_nopunct = text_nopunct.replace(' ', replace_with)
    return text_nopunct 


def normalize(text):
    text = urllib2.unquote(text.strip()).lower()
    return text_filter(text, replace_with = '')
    

def ligth_normalize(text):
    return urllib2.unquote(text.strip()).lower()


def match(text, texts, normalizator=lambda x:x):
    """Returns this entry from texts that best matches text according to Levenstein distance."""
    if len(texts)<=0: raise "[text.match] matching %s with empty list!" % (text)
    if len(texts)==1: return texts[0]

    matched, mindistance = texts[0], 1000000
    for other in texts:
        distance = nltk.metrics.edit_distance(text, other)
        if distance < mindistance:  matched, distance = other, mindistance
    return matched


def build_approximate2exacts(exact_values, approximator=normalize):
    """Constructs index (dictionary {approximate-value: list of values}) for fast approximate matching."""    
    approximate2exact = {}
    for ye in exact_values:
        approximate2exact.setdefault(approximator(ye), list()).append(ye)
    return approximate2exact





            


