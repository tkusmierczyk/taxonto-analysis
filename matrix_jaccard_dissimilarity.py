#!/usr/bin/python
# -*- coding: utf-8 -*-


from utils import *
import sys


def matrix_jaccard_dissimilarity_weighting(kk2c):
    """Takes matrix of counts {(key1,key2): count} and prints out symmetric matrix of weights 1-JI."""
    if not check_pairdict2weight_symmetric(kk2c): raise Exception("The matrix must be symmetric!")
    kk2w = {}
    keys = extract_pairdict2weight_keys(kk2c)
    for k1 in keys:
        for k2 in keys:
            weight = 1.0 - float(kk2c[(k1,k2)]) / (kk2c[(k1,k1)] + kk2c[(k2,k2)] - kk2c[(k1,k2)])
            kk2w[(k1,k2)] = kk2w[(k2,k1)] = weight
    return kk2w


if __name__=="__main__":

    log.info("Loads matrix of counts and prints out symmetric matrix of dissimilarity 1-JaccardIndex.")

    try: src = sys.argv[1]
    except: log.err("Argument excpeted: src matrix file."); sys.exit(-1)
    try: dst = sys.argv[2]
    except: log.err("Argument excpeted: dst matrix file."); sys.exit(-1)

    kk2c = load_pairdict2weight(open(src))
    log.dbg("Src matrix is symmetric: %s " % check_pairdict2weight_symmetric(kk2c) )
    kk2w = matrix_jaccard_dissimilarity_weighting(kk2c) 
    store_pairdict2weight(open(dst,"w"), kk2w)
    log.dbg("Dst matrix is symmetric: %s " % check_pairdict2weight_symmetric(kk2w) )
    

