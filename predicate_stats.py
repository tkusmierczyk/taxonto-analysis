#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
from utils import *
import log
import threading


def predicate_pairwise_weight_const(p1_count, p2_count):
    if p1_count>0 and p2_count>0: return 1
    return 0


def predicate_pairwise_weight_intersect(p1_count, p2_count):
    return min(p1_count, p2_count)


def predicates_pairwise_count(p_counts, x_predcount, pp_count_weight = predicate_pairwise_weight_intersect):
    """x_predcount - {object: {predicate: count} }"""
    predicates = set(p_counts.keys())

    pp_count = {}    

    for i,p1 in enumerate(predicates):   
        log.dbg("%i processed out of %i" % (i, len(predicates)) )            
        for p2 in predicates:
            total_weight = 0
            for x,predicate2count in x_predcount.iteritems():
                p1_count = predicate2count.get(p1, 0)
                p2_count = predicate2count.get(p2, 0)
                weight   = pp_count_weight(p1_count, p2_count)
                total_weight += weight 
            pp_count[(p1,p2)] = total_weight
            pp_count[(p2,p1)] = total_weight

    return pp_count


def load_facts(input):
    """Reads from input file facts and returns tuple: 
        s_predicates, o_predicates, s_counts, p_counts, o_counts.

        Denote: s-subject, p-predicate, o-object.
        s_predicates = {subject: list-of-predicates}
        o_predicates = {object: list-of-predicates}"""
    s_predicates = {} #{subject: list-of-predicates}
    o_predicates = {} #{object: list-of-predicates}
    s_counts = {}
    p_counts = {}
    o_counts = {}
    for ix,row in enumerate( load_csv_rows(input) ):
        if ix%50000 == 0: log.dbg("%i rows processed..." % (ix))
        s,p,o = extract_spo(row)
        #s_predicates[s] = s_predicates.get(s,[])+[p]
        #o_predicates[o] = o_predicates.get(o,[])+[p]
        insert_dict2list(s_predicates, s, p)
        insert_dict2list(o_predicates, o, p)
        s_counts[s] = s_counts.get(s,0)+1
        p_counts[p] = p_counts.get(p,0)+1
        o_counts[o] = o_counts.get(o,0)+1
    return s_predicates, o_predicates, s_counts, p_counts, o_counts


def load_facts2(input):
    s_predicates, o_predicates, s_counts, p_counts, o_counts = load_facts(input)
    s_predicates = convert_dict2list_dict2dcounts(s_predicates) #{subject: {predicte:count} }
    o_predicates = convert_dict2list_dict2dcounts(o_predicates) #{object: {predicate:count} }
    return s_predicates, o_predicates, s_counts, p_counts, o_counts


if __name__=="__main__":

    log.info("Loads file with facts and writes counts of subjects/objects/predicates"+
             " and matrices of coocurrences of predicates.")

    try: path = sys.argv[1]       
    except: log.err("Arg expected: SPO-triples (e.g. yagoFacts.tsv) file."); sys.exit(-1);
    input = open(path)  

    s_predicates, o_predicates, s_counts, p_counts, o_counts = load_facts2(input)

    store_k2dc_dictionary(open(path+".s2p", "w"), s_predicates)
    store_k2dc_dictionary(open(path+".o2p", "w"), o_predicates)
    store_o2c_list( open(path+".s_count", "w"), sort_o2c_dictionary(s_counts) )
    store_o2c_list( open(path+".p_count", "w"), sort_o2c_dictionary(p_counts) )
    store_o2c_list( open(path+".o_count", "w"), sort_o2c_dictionary(o_counts) )

    
    pp_count = predicates_pairwise_count(p_counts, o_predicates)
    store_pairdict2weight(open(path+".pp_ocount", "w"), pp_count)

    pp_count = predicates_pairwise_count(p_counts, s_predicates)
    store_pairdict2weight(open(path+".pp_scount", "w"), pp_count)

