#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
from utils import *
import log


def retrieve_spo_counts(infile):
    log.dbg("loading s_counts, p_counts, o_counts from %s " % str(infile) )
    s_counts = {}
    p_counts = {}
    o_counts = {}
    for ix,row in enumerate( load_csv_rows(infile) ):
        if ix%50000 == 0: log.dbg("%i rows processed..." % (ix))
        s,p,o = extract_spo(row)
        s_counts[s] = s_counts.get(s,0)+1
        p_counts[p] = p_counts.get(p,0)+1
        o_counts[o] = o_counts.get(o,0)+1
    return  s_counts, p_counts, o_counts


def load_facts3(infile):
    log.dbg("loading entities' aspects from file %s " % str(infile) )    

    entity2aspects = {}
    aspect2entities = {}
    entity2predicates = {}
    predicate2entities = {}

    for ix,row in enumerate( load_csv_rows(infile) ):
        if ix%50000 == 0: log.dbg("%i rows processed..." % (ix))

        s,p,o = extract_spo(row)
        aspect = (p,o)

        insert_dict2set(entity2aspects, s, aspect)
        insert_dict2set(aspect2entities, aspect, s)
        insert_dict2set(entity2predicates, s, p)
        insert_dict2set(predicate2entities, p, s)

    return entity2aspects, aspect2entities, entity2predicates, predicate2entities
    

def extract_predicates_from_aspects(aspects):
    predicates = set(p for p,o in aspects)
    return list(predicates)


if __name__=="__main__":

    log.info("Loads file with facts and writes counts of subjects/objects/predicates.")

    try: path = sys.argv[1]
    except: log.err("Arg expected: SPO-triples (e.g. yagoFacts) file."); sys.exit(-1);

    log.info("Loading S/P/O counts...")             
    s_counts, p_counts, o_counts = retrieve_spo_counts( open(path) )


    store_o2c_list( open(path+".s_count", "w"), sort_o2c_dictionary(s_counts))
    store_o2c_list( open(path+".p_count", "w"), sort_o2c_dictionary(p_counts) )
    store_o2c_list( open(path+".o_count", "w"), sort_o2c_dictionary(o_counts) )
