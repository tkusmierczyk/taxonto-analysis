#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
from utils import *
import log



if __name__=="__main__":

    try: path = sys.argv[1]         
    except: log.err("Arg expected: file that has in each row: key[count] list-of-entities."); sys.exit(-1);
    
    try: mincount = int(sys.argv[2])
    except: log.err("Arg expected: mincount"); sys.exit(-1)

    numrows = 0
    no_smaller_than_mincount = 0
    for line in open(path).xreadlines():
        key = line.split()[0]
        count = int(key.split("[")[1].split("]")[0])
        #print key, count, mincount, type(count), type(mincount), (count >= mincount)

        if count >= mincount: no_smaller_than_mincount += 1
        numrows += 1

    log.info( "numrows = %i " % numrows)
    log.info( "no_smaller_than_mincount = %i " %  no_smaller_than_mincount)


