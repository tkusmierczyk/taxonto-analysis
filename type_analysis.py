#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
from utils import *
import log


def learn_types(f, type_predicate=is_type_predicate, \
                   two_types_predicate=is_two_types_predicate):
    """Loads from SPO-file f set of types."""
    types = set()
    for ix,row in enumerate( load_csv_rows(f) ):
        if ix%50000 == 0: log.dbg("%i rows processed..." % (ix))
        s,p,o = extract_spo(row)
        if two_types_predicate(p):
            types.add(s)
            types.add(o)
        elif type_predicate(p): 
            types.add(o)
    log.info("%i known types" % len(types))
    return types


def qualify_types(types):
    """Assigns each type to one of three groups: wordnet_types, wiki_types, other_types."""
    wordnet_types = set()
    wiki_types = set()
    other_types = set()
    for type1 in types:
        if type1.startswith("<wordnet"): wordnet_types.add(type1)
        elif type1.startswith("<wiki"):    wiki_types.add(type1)
        else: other_types.add(type1)
    return wordnet_types, wiki_types, other_types


def load_entities_types(infile, types):
    """Extracts from infile two dictionaries: type2entities, entity2types."""
    type2entities = {}
    entity2types = {}
    for ix,row in enumerate( load_csv_rows(infile) ):
        if ix%50000 == 0: log.dbg("%i rows processed..." % (ix))
        s,p,o = extract_spo(row)
        if  (s in types) or (o not in types): continue
        insert_dict2set(type2entities, o, s)
        insert_dict2set(entity2types, s, o)
    return type2entities, entity2types



if __name__=="__main__":

    log.info("Loads file with types assignment and prints out type2entities/entity2types files.")

    try: path = sys.argv[1]         
    except: log.err("Arg expected: SPO-triples (e.g. yagoTypes) file."); sys.exit(-1);

    log.info("Learning names of types")
    types = learn_types(open(path))

    wordnet_types, wiki_types, other_types = qualify_types(types)
    log.info("%i of wordnet types: %s" % (len(wordnet_types),str(wordnet_types)[:400]) )
    log.info("%i of wiki types: %s" % (len(wiki_types),str(wiki_types)[:400]) )
    log.info("%i of other types: %s" % (len(other_types),str(other_types)) )


    log.info("Calculating number of non-types being in relation with types (type vs. list of entities)...")    
    type2entities, entity2types = load_entities_types( open(path), types)          
    log.info("len(type2entities)=%i" % len(type2entities))
    log.info("len(entity2types)=%i" % len(entity2types))

    store_k2l_dictionary(open(path+".type2entities", "w"), type2entities)
    store_k2l_dictionary(open(path+".entity2types", "w"), entity2types)

        

