#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
import log
from utils import *
from type_analysis import *
from spo_count import *


if __name__=="__main__":    

    log.info("For given type finds the entity that has the biggest number of facts assigned")

    try: types_path = sys.argv[1]         
    except: log.err("Arg expected: SPO-triples (e.g. yagoTypes) file."); sys.exit(-1);

    try: facts_path = sys.argv[2] 
    except: log.err("Arg expected: SPO-triples (e.g. yagoFacts) file."); sys.exit(-1);

    log.info("Learning names of types from %s" % types_path)
    types = learn_types(open(types_path))

    log.info("Calculating (type vs. list of entities) structures from %s" % types_path)    
    type2entities, entity2types = load_entities_types( open(types_path), types )          
    log.info("len(type2entities)=%i" % len(type2entities))
    log.info("len(entity2types)=%i" % len(entity2types))

    log.info("Loading S/P/O counts from %s" % facts_path)             
    s_counts, p_counts, o_counts = retrieve_spo_counts( open(facts_path) )

    log.info("The most popular subjects:")
    c2s = list( sorted( list( (c,s) for s,c in s_counts.iteritems() ), reverse=True ) )
    for c,s in c2s[:100]:
        log.info("%i %s %s" % (c,s,str(entity2types.get(s,[]))) )        

    #sys.exit(0)

    log.info("The most interesting subjects for type:")
    while True:
        sys.stdout.write("Enter type name:")    
        type_name = sys.stdin.readline().strip()
        if type_name not in type2entities: sys.stdout.write("Unknown type name!\n"); continue

        count2entity = list( (s_counts.get(e,0),e) for e in type2entities[type_name] )
        for c,e in list( sorted(count2entity, reverse=True) )[:200] :
            sys.stdout.write("%i %s\n" % (c,e))    
    

