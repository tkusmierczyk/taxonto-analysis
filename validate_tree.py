#!/usr/bin/python
# -*- coding: utf-8 -*-


import log
from type_analysis import *
from build_types_tree import *


def print_predecessor(f, type1, type2supertypes):
    f.write("[--------"+str(type1)+"----------]\n")
    supertypes = set( type2supertypes.get(type1,[]) )
    while len(supertypes) > 0:
        nextlevel_supertypes = set()
        f.write(">> ")
        for sup in supertypes:
            f.write(" %s" % str(sup))
            nextlevel_supertypes.update( type2supertypes.get(sup,[]) )
        f.write("\n")
        supertypes = nextlevel_supertypes


def first_common_predecessor(type1, type2, type2supertypes, cache={}):

    if type1==type2: return type1,0
    if (type1,type2) in cache: return cache[(type1,type2)]
    if (type2,type1) in cache: return cache[(type2,type1)]
    
    predecessor, dist = None, 1000000

    for super1 in type2supertypes.get(type1, []):
        predecessorN, distN = first_common_predecessor(super1, type2, type2supertypes, cache)
        if distN+1 < dist:  predecessor, dist = predecessorN, distN+1
        
    for super2 in type2supertypes.get(type2, []):
        predecessorN, distN = first_common_predecessor(type1, super2, type2supertypes, cache)
        if distN+1 < dist:  predecessor, dist = predecessorN, distN+1

    if not predecessor is None: 
        cache[(type1,type2)] = predecessor, dist
        cache[(type2,type1)] = predecessor, dist

    return predecessor, dist


if __name__=="__main__":

    try: taxonomy_path = sys.argv[1]         
    except: log.err("Arg expected: SPO-triples (e.g. yagoTaxonomy) file."); sys.exit(-1);


    log.info("Learning names of types")
    types = learn_types(open(taxonomy_path))
       
    log.info("Building type/subClassOf graph of types")
    type2subtypes,type2supertypes,singletons = extract_subtypes( open(taxonomy_path), types)
    log.info("len(type2subtypes)=%i" % len(type2subtypes))
    log.info("len(type2supertypes)=%i" % len(type2supertypes))
    log.info("len(singletons)=%i" % len(singletons))


    log.info("Validating")
    cache = {}
    for i, (basetype, types) in enumerate(type2supertypes.iteritems()):
        if i%50000 == 0: log.dbg("%i out of %i " % (i, len(type2supertypes)) )
        types = list(types)
        for i,type1 in enumerate(types):
            for j in xrange(i+1, len(types)):
                type2 = types[j]

                predecessor, dist = first_common_predecessor(type1, type2, type2supertypes, cache)
                #log.dbg(str(basetype)+" -> "+str(type1)+" "+str(type2)+" -> "+str(predecessor)+" ("+str(dist)+")")

                if predecessor!=type1 and predecessor!=type2:
                    log.err("[No linear relation]"+str(basetype)+" -> "+str(type1)+" "+str(type2)+\
                                " -> "+str(predecessor)+" ("+str(dist)+")")
                    print_predecessor(sys.stderr, type1, type2supertypes)
                    print_predecessor(sys.stderr, type2, type2supertypes)

                if predecessor is None or dist>100:
                    log.err("[No common predescor]"+str(basetype)+" -> "+str(type1)+" "+str(type2)+\
                                " -> "+str(predecessor)+" ("+str(dist)+")")
    log.info("Done")


