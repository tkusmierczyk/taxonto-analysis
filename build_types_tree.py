#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
from utils import *
import log
from type_analysis import *


def extract_subtypes(infile, types):
    type2subtypes = {}
    type2supertypes  = {}
    for ix,row in enumerate( load_csv_rows(infile) ):
        if ix%50000 == 0: log.dbg("%i rows processed..." % (ix))
        s,p,o = extract_spo(row)
        if (s not in types) or (o not in types): continue
        insert_dict2set(type2subtypes, o, s)
        insert_dict2set(type2supertypes, s, o)
    singletons = set(t for t in types if (t not in type2subtypes) and (t not in type2supertypes) )
    return type2subtypes, type2supertypes, singletons


if __name__=="__main__":

    log.info("Loads taxononomy file and writes type2subtypes/type2supertypes/type_singletons files.")

    try: 
        path = sys.argv[1]         
    except: 
        log.err("Arg expected: SPO-triples (e.g. yagoTaxonomy) file."); sys.exit(-1);

    log.info("Learning names of types from file %s" % path)
    types = learn_types(open(path))
   
    log.info("Building type/subClassOf graph of types with file %s " % path)
    type2subtypes,type2supertypes,singletons = extract_subtypes( open(path), types )

    log.info("len(type2subtypes)=%i" % len(type2subtypes))
    log.info("len(type2supertypes)=%i" % len(type2supertypes))
    log.info("len(singletons)=%i" % len(singletons))

    store_k2l_dictionary(open(path+".type2subtypes", "w"), type2subtypes)
    store_k2l_dictionary(open(path+".type2supertypes", "w"), type2supertypes)
    store_list(open(path+".type_singletons", "w"), singletons)


    


