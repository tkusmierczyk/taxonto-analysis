#!/usr/bin/python
# -*- coding: utf-8 -*-

from spo_count import *
import log
import sys

if __name__=="__main__":
    log.info("Interactive analysis of entities' aspects.")

    try: facts_path = sys.argv[1]
    except: log.err("Arg expected: SPO-triples (e.g. yagoFacts) file."); sys.exit(-1);

    entity2aspects, aspect2entities, entity2predicates, predicates2entities = load_facts3( open(facts_path) )
    
    log.info("Aspects' analysis for an entity:")
    while True:
        sys.stdout.write("Enter entity name:")    
        name = sys.stdin.readline().strip()
        if name not in entity2aspects: sys.stdout.write("Unknown entity name!\n"); continue

        aspects = entity2aspects[name]
        for aspect in aspects:        
            p,o = aspect
            entities = aspect2entities[aspect]
            sys.stdout.write("p:%s o:%s numentities:%i\n" % (p,o, len(entities)))    
            for i,entity in enumerate(entities): 
                if i>10: sys.stdout.write("  ...\n"); break
                sys.stdout.write("  %i.\t%s\n" % (i,entity) )
            
    
