#!/usr/bin/python
# -*- coding: utf-8 -*-


import log
import sys
from spo_count import * 
from utils import *


def load_entity2weight(infile):
    lines = (line.strip() for line in infile.xreadlines() if line.strip()!="" and not line.strip().startswith("#"))
    entity2weight = {}
    for line in lines:
        try:
            try:
                line    = line.decode('unicode-escape')
            except:
                log.err("FAILED TO DECODE %s"  % line)
            parts   = line.lower().split()
            entity  = "<"+reduce(lambda p1,p2: p1+"_"+p2, (p for p in parts[:-1]) )+">"
            weight  = float(parts[-1])
            entity2weight[entity] = weight
        except Exception as e:
            log.err("FAILED TO PARSE LINE %s: %s" % (line, str(e)) )
    return entity2weight



if __name__=="__main__":

    try: path = sys.argv[1]
    except: log.err("Input two column (entity id, weight file) expected!"); sys.exit(-1)

    try: facts_path = sys.argv[2]         
    except: log.err("Arg expected: SPO-triples (e.g. yagoFacts) file."); sys.exit(-1);

    try: entities_path = sys.argv[3]
    except: log.err("Arg expected: file having an entity to be considered in each line."); sys.exit(-1);

    try: outdir = sys.argv[4]
    except:  log.err("Arg expected: output directory expected"); sys.exit(-1)



    
    log.info("Loading entites to be considered from file %s" % entities_path)
    lines = ( line.strip() for line in open(entities_path).readlines() )
    consider_entities = list(line for line in lines if line!="" and not line.startswith("#") )
    log.info("Entities to be considered: %s" % str(consider_entities)[:400] )

    log.info("Loading entities and weights")
    entity2weight = load_entity2weight(open(path, "r"))
    log.dbg("entity2weight[%i] = %s" % (len(entity2weight), k2w_str(entity2weight, limit=1000)) )


    log.info("Loading facts from %s " % facts_path)
    entity2aspects, aspect2entities, entity2predicates, predicates2entities = load_facts3( open(facts_path) )


    weights = entity2weight.values()
    log.info("%i weights" % len(weights))
    log.info("min = %.10f" % min(weights))
    log.info("max = %.10f" % max(weights))

    
    
    log.info("Building weight files for entities")
    for entity in consider_entities:    
        if entity not in entity2predicates:
            log.err("Entity %s is not known! Skipping..." % str(entity) )
            continue

        #entity descrition
        predicates  = entity2predicates[entity]
        aspects     = entity2aspects[entity]
        log.info("ENTITY=%s: PREDICATES[%i]=%s ASPECTS[%i]=%s" % (entity, len(predicates), str(predicates)[:2000], len(aspects), str(aspects)[:2000] ) )    
            
        #entity aspects' weights
        aspect2weight = {}
        for aspect in aspects:
            p,o = aspect
            if o not in entity2weight:
                log.warn("%s not found in entity2weight!" % o) 
                continue
            aspect2weight[aspect] = entity2weight[o]


        #normalization of weights
        weights_sorted = list(sorted(set(aspect2weight.values())))
        try:
            new_min_val = weights_sorted[1]-weights_sorted[0]
        except:
            log.err("Failed to calculate new_min_val. Using default!")
            new_min_val = 1.0/len(aspects)

        try:
            l = min(aspect2weight.values()) - new_min_val
            u = max(aspect2weight.values())
            log.dbg(" new min  val = %.12f" % new_min_val )
            aspect2weight = dict( (a,float(w-l)/(u-l)) for a,w in aspect2weight.iteritems())
            log.dbg("aspect2weight[%i] = %s" % (len(aspect2weight), k2w_str(aspect2weight, inv=True, limit=1000)) )
            default_weight = min(aspect2weight.values()) / 10.0
        except: 
            log.err("Failed to rescale aspects weights!")
            aspect2weight = {}
            default_weight = 1.0/len(aspects)

        
        #storing results
        outpath = outdir + "/" + entity_name_format(entity) +"_weights" 
        log.info("Storing weights for %i aspects in %s with default weight = %.12f " % (len(aspects), outpath, default_weight) )
        out = open(outpath, "w")   
        for aspect in sorted(aspects):
            p,o = aspect
            weight = aspect2weight.get(aspect, default_weight)
            out.write("%s:%s\t%.12f\n" % (p,o,weight) )
        out.close()
            
    
        
    
