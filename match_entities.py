#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Matching entities' names using approximate index."""

import sys
from utils import *
import log
from type_analysis import *
import text
import urllib2


def match_using_approximate_index(entities, approximate2entities, approximator=text.normalize, normalizator=text.ligth_normalize):
    """Matches entities to entities from set of values of approximate2entities.

    approximator - function that was used for approximate2entities construction
    normalizator - how names should be normalized for exact match
    """
    entity2matched, missed = dict(), list()
    for exact in entities:
        approximate = approximator(exact)
        if approximate not in approximate2entities: 
            missed.append(exact)
        else:
            possible_entities = approximate2entities[approximate]
            entity2matched[exact] = text.match(exact, possible_entities, normalizator) #levenstein used for selecting best option
    return entity2matched, missed


def extract_entitiy2types(entity2matched, matched2types):
    return dict( (entitiy, matched2types[matched]) for entitiy,matched in entity2matched.iteritems() )


if __name__=="__main__":

    log.info("Loads file with yago-entities (and types) and tries to match entites from other file.")

    try: path = sys.argv[1]         
    except: log.err("Arg expected: SPO-triples (e.g. yagoTypes) file."); sys.exit(-1);

    try: entities_path = sys.argv[2]         
    except: log.err("Arg expected: file with entity name in each line."); sys.exit(-1);

    #########################################################

    log.info("Loading list of entites from file %s" % entities_path)
    entities = list(line.strip() for line in open(entities_path).xreadlines() if line.strip()!="" and line[0]!="#")
    log.info("%i entities loaded" % len(entities))

    #########################################################

    log.info("Learning names of types")
    types = learn_types(open(path))

    wordnet_types, wiki_types, other_types = qualify_types(types)
    log.info("%i of wordnet types: %s" % (len(wordnet_types),str(wordnet_types)[:400]) )
    log.info("%i of wiki types: %s" % (len(wiki_types),str(wiki_types)[:400]) )
    log.info("%i of other types: %s" % (len(other_types),str(other_types)) )

    log.info("Calculating number of non-types being in relation with types (type vs. list of entities)...")    
    yago_type2entities, yago_entity2types = load_entities_types( open(path), types)          
    log.info("len(yago_type2entities)=%i" % len(yago_type2entities))
    log.info("len(yago_entity2types)=%i" % len(yago_entity2types))

    log.info("Building index {yago approximation: list of exact entities names}")
    yago_approximate2entities = text.build_approximate2exacts(yago_entity2types.keys(), approximator=text.normalize)

    #########################################################

    log.info("Matching external entities to yago entities")
    entity2matched, missed = match_using_approximate_index(entities, yago_approximate2entities)
    log.info("%i matched, %i missed" % (len(entity2matched),len(missed)))
    
    log.info("Matching external entities to yago types")
    entity2types = extract_entitiy2types(entity2matched, yago_entity2types)
    log.info("len(entity2types) = %i" % len(entity2types))

    store_o2c_dict(open(entities_path+".matched", "w"), entity2matched)
    store_list(open(entities_path+".missed", "w"), missed)
    store_k2l_dictionary(open(entities_path+".matched.types", "w"), entity2types)

