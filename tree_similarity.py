#!/usr/bin/python
# -*- coding: utf-8 -*-


import log
from type_analysis import *
from build_types_tree import *
from validate_tree import *
from predicate_stats import *
from spo_count import * 
from utils import *


###################################################################################


def bfs(start_node, node2layer, max_num_hops = 10000000):
    """Walks in DAG and gather all nodes reachable from start_node.

    Returns dictionary {node: min number of edges to 'start_node'}
    """
    node2numhops    = {}                                                    #all nodes farther in the hierarchy
    hops            = 0                                                     #num hops farther
    layer           = set( node2layer.get(start_node,[]) )

    while len(layer) > 0:                                                   #as long as we can go farther

        hops        = hops + 1
        if hops > max_num_hops: break                                       #can not go to far

        nextlevel_layer = set()
        for node in layer:                                                  #for each in next layer            
            if node in node2numhops: continue                               #already seen -> skip
            nextlevel_layer.update( node2layer.get(node,[]) )               #walk farther
            node2numhops[node] = hops                                       #mark as seen and store num hops
        layer  = nextlevel_layer

    return node2numhops



def get_type_all_supertypes(type1, type2supertypes, maxnumhops=1000000):
    """Walks up the hierarchy and gather all supertypes (e.g. roots of subtrees) of 'type1'.

    Returns dictionary {supertype: min number of edges to 'type1'}
    """
    return bfs(type1, type2supertypes, maxnumhops)


def get_type_all_subtypes(type1, type2subtypes, maxnumhops=1000000):
    """Walks down the hierarchy and gather all subtypes of 'type1'.

    Returns dictionary {subtype: min number of edges to 'type1'}
    """
    return bfs(type1, type2subtypes, maxnumhops)



def calc_type_depth(type1, type2supertypes):
    """Returns the length (number of edges) of a shortest path from root to the node 'type1'. 

    type2supertypes = {type: it's direct parents}
    """
    depth = 0                                                               
    supertypes = set( type2supertypes.get(type1,[]) )

    while len(supertypes) > 0:

        depth       = depth + 1
        nextlevel_supertypes = set()
        for sup in supertypes:
            if sup not in type2supertypes: break    #if has not any supertype == it's supertype is a root
            nextlevel_supertypes.update( type2supertypes.get(sup,[]) )
        supertypes  = nextlevel_supertypes

    return depth+1


def calc_type2depth(type2supertypes):
    """Returns dictionary{type: depth-in-hierarchy}."""
    type2depth = {}
    for i,type1 in enumerate(types):
        if i%50000==0: log.dbg("%i processed out of %i..." % (i,len(types)))
        type2depth[type1] = calc_type_depth(type1, type2supertypes)
    return type2depth

def select_types_mindepth(types, mindepth, type2depth):
    """Returns dictionary{type: depth} of a types for which depth>=mindepth."""
    return dict( (t, type2depth[t]) for t in types if type2depth[t] >= mindepth )


###################################################################################

def sup_in_radius(type1, radius, type2supertypes, type2subtypes, type2depth, neigh2dist = {}):
    """Finds neighbours of type1 in subtrees starting from supertypes of type1."""

    #retrieve supertypes of type1
    sup2numhops = get_type_all_supertypes(type1, type2supertypes)
    log.info(">>>>>> type=[%s] supertype2numhops: %s" % (str(type1), str(list(sup2numhops.iteritems()))[:500]) )    

    #consider subtrees starting from supertypes of type1
    for sup, numhops_up in sup2numhops.iteritems():

        sup_depth = type2depth[sup]
        dist1 = float(numhops_up) / (sup_depth+numhops_up)
        if dist1 > radius: continue #skip whole subtree if there is no chance to be in neighbourhood
        log.dbg("storing sup=%s with dist1=%f" % (sup, dist1) )
        neigh2dist[sup] = min(dist1, neigh2dist.get(sup,INF)) #(*)
        
        max_numhops_down    = int( float(sup_depth*radius) / (1.0 - radius) )
        sub2numhops         = get_type_all_subtypes(sup, type2subtypes, max_numhops_down)            
        neighs		    = 0
        for sub,numhops_down in sub2numhops.iteritems():
            if sub in sup2numhops: continue #skipping if in the same path as type1: see (*)
            
            dist2       = float(numhops_down) / (sup_depth+numhops_down)
            dist        = max(dist1, dist2)
            #log.dbg("considering sub=%s numhops_down=%i sup=%s numhops_up=%i sup_depth=%i ==> "+
            #        "dist1=%f dist2=%f" % (sub, numhops_down, sup, numhops_up, sup_depth, dist1, dist2) )
            neigh2dist[sub]     = min(dist, neigh2dist.get(sub,INF))
            #log.dbg("storing neigh=%s (of sup=%s) with dist=%f" % (sub, sup, dist) )
    	    neighs 	= neighs + 1
    	log.dbg("%i neighs stored for sup=%s" % (neighs, sup) )

    return neigh2dist


def sub_in_radius(type1, radius, type2subtypes, type2depth, neigh2dist = {}):
    """Find subtypes of type1 in radius."""

    depth1 = type2depth[type1]
    sub2numhops = get_type_all_subtypes(type1, type2subtypes)
    log.info(">>>>>> type=[%s] (depth=%i) sub2numhops: %s" % (str(type1), depth1, str(list(sub2numhops.iteritems()))[:500]) )        

    for sub,numhops_down in sub2numhops.iteritems():
        dist1 = float(numhops_down) / (depth1+numhops_down)
        if dist1 > radius: continue
        log.dbg("storing sub=%s with dist1=%f" % (sub, dist1) )
        neigh2dist[sub] = min(dist1, neigh2dist.get(sub,INF))

    return neigh2dist



def neighbours_in_radius(type1, radius, type2supertypes, type2subtypes, type2depth):
    """Returns dictionary{type:distance}  for neighbours (distance<=radius) of type1."""

    log.dbg(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    log.dbg(">>> LOOKING FOR NEIGHBOURS OF TYPE=%s IN RADIUS=%f" % (str(type1), radius) )
    neigh2dist = {}
    
    neigh2dist[type1] = 0.0 #distance to itself is equal 0.0 # do not remove this unless you know why it is here!
    neigh2dist = sup_in_radius(type1, radius, type2supertypes, type2subtypes, type2depth, neigh2dist)
    neigh2dist = sub_in_radius(type1, radius, type2subtypes, type2depth, neigh2dist)       

    log.info("<<< NEIGHBOURS OF TYPE=%s IN RADIUS=%f NEIGH2DIST=%s" % (str(type1), (radius), k2w_str(neigh2dist, inv=False) ) )
    log.dbg("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")

    return neigh2dist    

###################################################################################

def _convert_type2dist_to_type2weight_(type2dist, sigma):
    w = lambda dist: gaussian_density(0.0, sigma, dist) #gaussian weighting
    #w = lambda dist: 1.0-dist #simple weighting
    return dict( (type1,w(dist)) for type1,dist in type2dist.iteritems() ) 

def _get_type2weight_over_types_(types, radius, type2supertypes, type2subtypes, type2depth):

    sigma = float(radius)/3.0 #cutting off less than 0.2% of important samples
    log.dbg("Calculating type2weight with sigma=%f" % sigma)

    type2weight = {}
    for type1 in types:
        type2dist   = neighbours_in_radius(type1, radius, type2supertypes, type2subtypes, type2depth)
        t2w         = _convert_type2dist_to_type2weight_(type2dist, sigma)
        for t,w in t2w.iteritems():
            type2weight[t] = max(w, type2weight.get(t,-INF))
    return type2weight

############################################

def get_weighted_entities(type2entities, type2weight):
    """Returns dictionary{entity: weight} for entities of types from dictionary{type: weight}."""
    entity2weight = {}
    for type1,weight in type2weight.iteritems():

        type_entities = type2entities.get(type1, []) 
        if len(type_entities) <= 0: continue                    #skip types without entities

        weight_per_entity = float(weight) / len(type_entities)  #divide type weight per its entities
        for entity in type_entities:
            entity2weight[entity] = entity2weight.get(entity, 0.0) + weight_per_entity

    return entity2weight


###################################################################################
###################################################################################
###################################################################################
###################################################################################



def p1p1_pairwise_weight(predicate1, predicate2, entity2predicates, entity2weight): 
    """x_predcount - {object: {predicate: count} }"""    
    total_weight = 0.0
    having_both_weight = 0.0
    for entity, weight in entity2weight.iteritems():
        if entity not in entity2predicates: continue                #enitity has no predicates

        has_predicate1 = predicate1 in entity2predicates[entity]
        has_predicate2 = predicate2 in entity2predicates[entity]

        if (not has_predicate1) and (not has_predicate2): continue  #entity has neither p1 nor p2

        total_weight = total_weight + weight
        if  has_predicate1  and  has_predicate2:                    #entity has both p1 and p2
            having_both_weight = having_both_weight + weight

    if total_weight == 0.0: 
        return None
    try:
        return 1.0 - float(having_both_weight) / total_weight           # 1 - WeightedJaccardIndex
    except Exception as e:
        log.err("EXCEPTION: %s" % str(e))
        return None


def select_entities_having_predicates(x2entities, entity2predicates, predicates):
    """
    x2entities - {sthing: list-of-entities}
    entity2predicates - {entity: list-of-its-predicates}
    predicates - set of predicates entity must have

    returns x2entities but only with entities having at least one of predicates
    """
    predicates = set(predicates)
    x2e = {}
    for x,entities in x2entities.iteritems():
        x2e[x] = list( e for e in entities if (e in entity2predicates) and len(set(entity2predicates[e]).intersection(predicates))>0 ) 
    return x2e
    

def select_entities_having_2predicates(x2entities, entity2predicates, p1, p2):
    """
    x2entities - {sthing: list-of-entities}
    entity2predicates - {entity: list-of-its-predicates}
    p1,p2 - set of predicates entity must have

    returns x2entities but only with entities having at least one of predicates
    """
    entity_valid = lambda e: (e in entity2predicates) and ((p1 in entity2predicates[e]) or (p2 in entity2predicates[e]))
    return dict( (x, list(e for e in entities if entity_valid(e))) for x,entities in x2entities.iteritems() )




def _x2entities_count_(x2entities):
    return sum( len(entities) for x, entities in x2entities.iteritems() )


def _pp_pairwise_weight_(p1, p2, entity, entity2predicates, predicate2entities, type2entities, type2weight):

    #filter out entities to be kept
    log.dbg("%i entities in %i types to be considered" % (_x2entities_count_(type2entities),len(type2entities)) )
    having = predicate2entities.get(p1, set()).union( predicate2entities.get(p2, set()) ) #entities having p1 or p2
    type2selected_entities  = dict( (t,having.intersection(e)) for t,e in type2entities.iteritems() )
    log.dbg("%i entities in %i types having %s or %s kept" % (_x2entities_count_(type2selected_entities), len(type2selected_entities), p1, p2) )

    #calculate weights of entities
    entity2weight           = get_weighted_entities(type2selected_entities, type2weight)
    log.dbg("len(entity2weight)=%i entity2weight=%s" % (len(entity2weight), k2w_str(entity2weight, limit=200)) )
    log.dbg("histogram of weight=%s" % str(hist(entity2weight.values())) )

    #remove considered entity to not influence
    try: entity2weight.pop(entity)
    except: log.err("Failed removing entity %s from its neighbourhood!" % entity)

    #calculate weighted dissimilarity
    dis = p1p1_pairwise_weight(p1, p2, entity2predicates, entity2weight)            
    
    return dis


def pp_pairwise_weight(entity, entity2predicates, predicate2entities, type2entities, type2weight): 
    """Calculates predicate-predicate dissimilarity matrix for a given entity."""

    #entity2predicates = dict( (e,set(predicates)) for e,predicates in entity2predicates.iteritems() )
    predicates = list(entity2predicates[entity])
    log.dbg("ENTITY=%s PS=%s" % (entity,str(predicates)) )

    pp_dis = {}    
    for i,p1 in enumerate(predicates):                                  #for each p1
        log.dbg("%i processed out of %i" % (i, len(predicates)) )            

        for j in xrange(i, len(predicates)):                            #for each p2
            p2 = predicates[j]
            log.info(">>> CONSIDERING PAIR OF p1=%s p2=%s" % (p1,p2) )

            #calculate dissimilarity with one of three methods  
            if p1 == p2:    
                log.dbg("the same predicate considered: dis = 0")
                dis = 0.0 #to speed up we assume that result            
            else:
                #check whether there is anything to be searched for
                p1entities = predicate2entities.get(p1,set())
                p2entities = predicate2entities.get(p2,set())
                if len(p1entities.intersection(p2entities)) <= 1:   
                    log.dbg("no entites to be considered: dis is undefined")
                    dis = None 
                else:   
                    log.dbg("full procedure of dis calculation applied")
                    dis = _pp_pairwise_weight_(p1, p2, entity, entity2predicates, predicate2entities, type2entities, type2weight)

            #store dissimilarity 
            pp_dis[(p2,p1)] = pp_dis[(p1,p2)] = dis
            log.dbg("<<< PAIR OF p1=%s p2=%s -> DISSIMILAIRITY=%s" % (p1,p2,str(dis)) )

    return pp_dis


def aa_pairwise_weight(entity, entity2aspects, aspect2entities, type2entities, type2weight):    
    """Calculates aspect-aspect dissimilarity matrix for a given aspect."""
    return pp_pairwise_weight(entity, entity2aspects, aspect2entities, type2entities, type2weight)



##################################################################################

def aspect2str(a):
    p,o = a
    return p+":"+o

def store_aa2dis(output, aa2weight, separator=CSV_SEPARATOR): 
    log.dbg("writting dictionary { (aspect,aspect): weight } to %s" % str(output))

    pairdict2weight = {}
    for (a1,a2), weight in aa2weight.iteritems():
        pairdict2weight[(aspect2str(a1),aspect2str(a2))] = weight

    store_pairdict2weight(output, pairdict2weight, separator)






if __name__=="__main__":

    try: taxonomy_path = sys.argv[1]         
    except: log.err("Arg expected: SPO-triples (e.g. yagoTaxonomy) file."); sys.exit(-1);

    try: types_path = sys.argv[2]         
    except: log.err("Arg expected: SPO-triples (e.g. yagoTypes) file."); sys.exit(-1);

    try: facts_path = sys.argv[3]         
    except: log.err("Arg expected: SPO-triples (e.g. yagoFacts) file."); sys.exit(-1);

    try: entities_path = sys.argv[4]
    except: log.err("Arg expected: file having an entity to be considered in each line."); sys.exit(-1);

    try: radius = float(sys.argv[5])
    except: log.err("Arg expected: neighbourhood radius expected"); sys.exit(-1)

    try: outdir = sys.argv[6]
    except:  log.err("Arg expected: output directory expected"); sys.exit(-1)


    ##############################################################################

    log.info("LOADING DATA...")

    log.info("Neighbourhood radius: %f" % radius)
    log.info("Output directory: %s" % outdir)
    
    log.info("Loading entites to be considered from file %s" % entities_path)
    lines = ( line.strip() for line in open(entities_path).readlines() )
    consider_entities = list(line for line in lines if line!="" and not line.startswith("#") )
    log.info("Entities to be considered: %s" % str(consider_entities)[:400] )

    log.info("Loading names of types from %s" % taxonomy_path)
    types = learn_types(open(taxonomy_path))

    wordnet_types, wiki_types, other_types = qualify_types(types)
    log.info("%i of wordnet types: %s"  % (len(wordnet_types),str(wordnet_types)[:400]) )
    log.info("%i of wiki types: %s"     % (len(wiki_types),str(wiki_types)[:400]) )
    log.info("%i of other types: %s"    % (len(other_types),str(other_types)) )

    
    log.info("Loading type/subClassOf graph of types from %s" % taxonomy_path)
    type2subtypes,type2supertypes,singletons = extract_subtypes( open(taxonomy_path), types)
    log.info("len(type2subtypes)=%i"    % len(type2subtypes))
    log.info("len(type2supertypes)=%i"  % len(type2supertypes))
    log.info("len(singletons)=%i"       % len(singletons))


    log.info("Loading facts from %s " % facts_path)
    entity2aspects, aspect2entities, entity2predicates, predicate2entities = load_facts3( open(facts_path) )

    log.info("Loading [type vs. list of entities] structures from %s " % types_path)    
    type2entities, entity2types = load_entities_types(open(types_path), types)
    log.info("len(type2entities)=%i"    % len(type2entities))
    log.info("len(entity2types)=%i"     % len(entity2types))

    ##############################################################################

    log.info("PROCESSING DATA...")

    log.info("Calculating depth")
    type2depth = calc_type2depth(type2supertypes)
    store_o2c_dict(open(taxonomy_path+".type2depth","w"), type2depth)

    log.info("Building dissimilarity matrices for entities")
    failures = []
    successes = []
    for entity in consider_entities:    
        if entity not in  entity2types:
            log.err("Entity %s is not known! Skipping..." % str(entity) )
            continue

        try:
            #entity descrition
            entity_types = entity2types[entity]
            aspects = entity2aspects[entity]
            log.info("ENTITY=%s: TYPES[%i]=%s ASPECTS[%i]=%s" % (entity, len(entity_types), str(entity_types)[:2000], len(aspects), str(aspects)[:2000] ) )    
            
            #weighting types
            log.dbg("WEIGHTING TYPES")
            type2weight = _get_type2weight_over_types_(entity_types, radius, type2supertypes, type2subtypes, type2depth)
            log.info("len(type2weight)=%i type2weight=%s" % ( len(type2weight), k2w_str(type2weight, limit=1000) ) )

            #pp dissmiliarity calculation
            log.dbg("PP DISSIMILARITY CALCULATION")
            pp_dis = pp_pairwise_weight(entity, entity2predicates, predicate2entities, type2entities, type2weight)
            log.dbg("pp dissimilarity matrix = %s " % str(list(pp_dis.iteritems()))[:400] )

            #Storing results
            outpath = outdir + "/" + entity_name_format(entity) +"_ppdissimilarity"
            store_pairdict2weight(open(outpath, "w"), pp_dis)


            #aa dissmiliarity calculation
            log.dbg("AA DISSIMILARITY CALCULATION")
            aa_dis = aa_pairwise_weight(entity, entity2aspects, aspect2entities, type2entities, type2weight)
            log.dbg("aa dissimilarity matrix = %s " % str(list(aa_dis.iteritems()))[:400] )


            log.dbg("FIXING EMPTY PLACES IN AA DISSIMILARITY")
            for aa,dis in aa_dis.iteritems():
                if dis == None:
                    a1,a2 = aa
                    p1,o1 = a1
                    p2,o2 = a2
                    fix = pp_dis[(p1,p2)]
                    aa_dis[aa] = fix
                    log.dbg("Fixing %s with %s,%s = %f" % (str(aa), str(p1), str(p2), fix) )


            #Storing results
            outpath = outdir + "/" + entity_name_format(entity) +"_aadissimilarity"
            store_aa2dis(open(outpath, "w"), aa_dis)

            successes.append(entity)
        except Exception as e:
            log.err("FAILED WITH ENTITY=%s: %s" % (entity, str(e)) )
            failures.append( (entity,e) )

    log.info("DONE.\n Failures: %s.\n Successes: %s" % (str(failures), str(successes)) )


