#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
from utils import *


if __name__=="__main__":
    inpath = sys.argv[1]
    outpath = sys.argv[2]

    fin = open(inpath)
    fout = open(outpath, "w")

    facts = list( line.strip().split()[0] for line in fin.xreadlines() )

    ff2w = {}
    for f1 in facts:
        for f2 in facts:
            ff2w[(f1,f2)] = 0.0

    store_pairdict2weight(fout, ff2w)



