#!/usr/bin/python
# -*- coding: utf-8 -*-


import log
import sys


if __name__=="__main__":

    try: path = sys.argv[1]
    except: log.err("Input file expected!"); sys.exit(-1)
    
    try: prefix = sys.argv[2]
    except: log.err("Output file expected!"); sys.exit(-1)


    log.info("Loading entites to be considered from file %s" % path)
    lines = ( line.strip() for line in open(path).readlines() )
    consider_entities = list(line for line in lines if line!="" and not line.startswith("#") )
    log.info("Entities to be considered: %s" % str(consider_entities)[:400] )

    for i,entity in enumerate(consider_entities):
        f = open("%s%.4i" % (prefix,i) , "w")
        f.write("%s\n" % entity)
        f.close()
