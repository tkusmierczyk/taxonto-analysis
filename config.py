
## YAGO:
#CSV_SEPARATOR = "\t"
#S_COL = 1
#P_COL = 2
#O_COL = 3
#TYPE = "type"
#SUBCLASSOF = "subclassof"


## DBPEDIA:
CSV_SEPARATOR = " "
S_COL = 0
P_COL = 1
O_COL = 2
TYPE = "<http://purl.org/dc/terms/subject>"
SUBCLASSOF = "<http://www.w3.org/2004/02/skos/core#broader>"

