#!/usr/bin/python
# -*- coding: utf-8 -*-


import log
import sys
from calc_aspects_weights import *

import matplotlib.pyplot as plt

import codecs


if __name__=="__main__":
    log.info("Plots histogram of weights from two column (id, weight) file.")

    try: path = sys.argv[1]
    except: log.err("Input file name expected!"); sys.exit(-1)

    entity2weight = load_entity2weight(codecs.open(path, "r", "utf-8"))
    #print entity2weight        
    weights = entity2weight.values()

    log.info("%i weights" % len(weights))
    log.info("min = %.10f" % min(weights))
    log.info("max = %.10f" % max(weights))
    
    plt.hist(weights, bins=100)
    plt.xlabel("Weight")
    plt.ylabel("Count")
    plt.show()
    
